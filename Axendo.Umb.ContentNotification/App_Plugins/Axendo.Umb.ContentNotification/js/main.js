$(window).load(
    function () {
        var injector = null;

        function addWrapperAndLogic() {
            if ($(".umb-panel-header").length) {
                if ($("#content-notification-container").length == 0) {
                    $("#umbracoMainPageBody").append('<div id="content-notification-container"></div>');
                    var html = $('<div id="content-notification" ng-controller="NodeEditsController"></div>');
                    injector.invoke(function ($compile) {
                        var obj = $('#content-notification-container'); // get wrapper
                        var scope = obj.scope(); // get scope
                        // generate dynamic content
                        if (scope != undefined) {
                            obj.html(html);
                            // compile!!!
                            $compile(obj.contents())(scope);
                        }
                    });
                }
            } else {
                setTimeout(addWrapperAndLogic, 1000);
            } 
        };


        function checkScope() {
            var scope = angular.element('#umbracoMainPageBody').scope();
            if (scope == undefined) {
                setTimeout(checkScope, 100);
            }
            else {
                injector = angular.element('#umbracoMainPageBody').injector();
                scope.$on('$viewContentLoaded', function () {
                    addWrapperAndLogic();
                });
            }
        }

        checkScope();
    }
);  

/**
 @ngdoc service
 * @name umbraco.services.nodeEditsNotificationsService
 *
 * @description
 * 
 */
(function () {
    'use strict';

    function nodeEditsNotificationsService(notificationsService, localizationService) {

        var localizedLabels = {};

        function setCurrentEditNotification(edit) {
            var allOpenNotifications = notificationsService.getCurrent();
            //Make sure we dont get multiple of the same messages
            if (_.where(allOpenNotifications, { contentNotification: true, userId: edit.UserId }).length > 0) {
                return;
            }

            notificationsService
                .add({
                    headline: localizedLabels.attention,
                    message: formatDictionaryItem(localizedLabels.userIsEditing, [edit.UserName]),
                    type: 'warning',
                    sticky: true,
                    contentNotification: true,
                    userId: edit.UserId
                });
        };

        function setPublishedNotification(email, time) {
            notificationsService
                .add({
                    headline: localizedLabels.nodePublished,
                    message: formatDictionaryItem(localizedLabels.userPublished, [email, time]),
                    type: 'info',
                    sticky: false,
                    contentNotification: true
                });
        };

        function setReleasedNotification(edit) {
            this.removeByUserId(edit.UserId);

            notificationsService
                .add({
                    headline: localizedLabels.released,
                    message: formatDictionaryItem(localizedLabels.userStoppedEditing, [edit.UserName]),
                    type: 'info',
                    sticky: false
                });
        };

        function removeByUserId(userId) {
            var allOpenNotifications = notificationsService.getCurrent();
            _.each(_.where(allOpenNotifications, { contentNotification: true, userId: userId }), function (el, index) {
                notificationsService.remove(index);
            });
        };

        function removeAll() {
            //Remove all old 'contentNotification' notifications
            var allOpenNotifications = notificationsService.getCurrent();
            _.each(_.where(allOpenNotifications, { contentNotification: true }), function (el, index) {
                notificationsService.remove(index);
            });
        }

        function formatDictionaryItem(value, tokens) {
            if (value) {
                if (tokens) {
                    for (var i = 0; i < tokens.length; i++) {
                        value = value.replace("%" + i + "%", tokens[i]);
                    }
                }
                return value;
            }
            return "[" + value + "]";
        }

        localizationService.localizeMany([
            "nodeEdits_attention", "nodeEdits_released", "nodeEdits_nodePublished",
            "nodeEdits_userIsEditing", "nodeEdits_userPublished", "nodeEdits_userStoppedEditing"
        ]).then(function (res) {
            localizedLabels.attention = res[0];
            localizedLabels.released = res[1];
            localizedLabels.nodePublished = res[2];
            localizedLabels.userIsEditing = res[3];
            localizedLabels.userPublished = res[4];
            localizedLabels.userStoppedEditing = res[5];
        });


        var service = {
            setCurrentEditNotification: setCurrentEditNotification,
            setPublishedNotification: setPublishedNotification,
            setReleasedNotification: setReleasedNotification,
            removeByUserId: removeByUserId,
            removeAll: removeAll
        };

        return service;
    };

    angular.module('umbraco.services').factory('nodeEditsNotificationsService', nodeEditsNotificationsService);

})();

﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Auditing;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Models.Identity;
using Umbraco.Core.Publishing;
using Umbraco.Core.Security;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace Axendo.Umb.ContentNotification.Hubs
{
    public class NodeEditsEventHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Published += ContentService_Published;
            BackOfficeUserManager.LogoutSuccess += BackOfficeUserManager_LogoutSuccess;
        }

        private void BackOfficeUserManager_LogoutSuccess(object sender, EventArgs e)
        {
            var args = e as IdentityAuditEventArgs;
            if (args.PerformingUser != -1)
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<NodeEditsHub>();
                NodeEditsHub.UserLogout(args.PerformingUser, hubContext);
            }
        }

        private void ContentService_Published(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            var currentUser = UmbracoContext.Current.Security.CurrentUser;
            if (currentUser == null)
            {
                return;
            }

            var email = currentUser.Email;
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<NodeEditsHub>();
            foreach (var node in e.PublishedEntities)
            {
                hubContext.Clients.Group(NodeEditsHub.EditGroup).broadcastPublished(node.Id, currentUser.Id, currentUser.Name, DateTime.Now.ToString("HH:mm:ss"));
            }
        }
    }
}

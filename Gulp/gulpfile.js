﻿/// <binding ProjectOpened='watch' />
'use strict';

var gulp = require('gulp');

var paths = {
    app_plugins: ['../Axendo.Umb.ContentNotification/App_Plugins/**/*.*']
};

gulp.task('app_plugins',
    function () {
        gulp.src(paths.app_plugins)
            .pipe(gulp.dest('../Axendo.Umb.ContentNotification.Site/App_Plugins/'));
    });

gulp.task('watch', ['app_plugins'], function () {
    gulp.watch(paths.app_plugins, ['app_plugins']);
});

